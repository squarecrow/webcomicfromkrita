#!/usr/bin/env python2

# started 2017-12-02
"""
Takes comics as Krita document files (*.KRA),
looks inside for text layers named after languages,
and outputs each language's text as SVG,
overlaid on the base comic.
"""
import zipfile, os, argparse
from StringIO import StringIO
from lxml import etree
import copy

parser = argparse.ArgumentParser( description = __doc__ )
parser.add_argument( "-f", "--file",
  help= "Krita file archive.",
  action= "store", dest= "filename")
  
parser.add_argument( "-o", "--imageout",
  help="""
  Output path of image that will be
  used in SVG structure.
  """, action= "store", dest= "imageoutput" )
  
args = parser.parse_args()
filename = args.filename
mainpath = os.path.dirname(filename)
basename = os.path.basename

print 'extracting content of',filename,'...'
krafile = zipfile.ZipFile(filename, 'r')

def noprefix(arg):
  """
  Removes namespace prefixes from tags and attributes.
  Example: <{prefixA}TAG {prefixB}KEY="VALUE">
  Returns: <TAG KEY="VALUE">
  'arg' must be a parsed xml element.
  All children of 'arg' will be modified as well.
  """
  def afterns(string):
    """ Count to '}', add 1, return rest of string. """
    return string[(string.find('}')+1):]
    
  for all_tags in arg.getiterator():
    all_tags.tag = afterns(all_tags.tag)
    for attr in all_tags.attrib:
      old = all_tags.attrib[attr]
      value = old
      del old
      key = afterns(attr)
      all_tags.attrib[key] = value
  return arg
  
def filexmltree(arg):  
  with krafile.open(arg+'.xml') as xml:
    filexml = etree.parse(xml).getroot()
  return noprefix(filexml)
  
maindoc = filexmltree('maindoc')  
docinfo = filexmltree('documentinfo')

class image():
  """ Miscellaneous image properties. """
  coords = ('width','height')
  tag  = maindoc.find(".//IMAGE")
  def __init__(S):
    S.size  = map(S.tag.attrib.get,S.coords)
    S.size  = tuple(map(int,S.size))
    S.name  = S.tag.attrib.get('name')
    S.title = S.name if S.name else basename
    
abstract = docinfo.find(".//abstract").text.splitlines()
abstract = '\n'.join([(' '*4) + i for i in abstract])

imageout = mainpath + args.imageoutput
print "Writing mergedimage.png to", imageout

with open(imageout, "w") as png:
  png.write(krafile.read("mergedimage.png"))

img = image()

def edic(ele,dic):
  """
  Set multiple attributes in element, with a dictionary.
  ele: xml element.
  dic: dictionary.
  """
  for key,val in dic.iteritems():
    ele.set(key,val)
  return ele
  
def ppiconvert(unit):
  """
  Convert PT units to PPI-appropriate pixel units.
  """
  ppi = int(img.tag.attrib.get('y-res'))
  return (unit * ppi) / 72
  
def scale(arg):
  return arg / 2

viewbox = tuple([0,0] + map(scale,img.size))

# TODO: remove xmlns when svg is part of an html document
# MAYBE: separate python script for HTML document
rootsvg = etree.Element("svg", {
  'xmlns'           : 'http://www.w3.org/2000/svg',
  'role'            : 'img',
  'aria-labelledby' : 'SVGdesc',
  'viewBox'         : ' '.join(map(str, viewbox)) } )

class textobject():
  def __init__(self,element):
    size = ['width','height']
    pos = ['x','y']
    def build(arg):
      # remove 'pt' at the end of the string with [:-2]
      arg = [element.get(i)[:-2] for i in arg]
      arg = map(scale, map(float,arg) )
      arg = map(int, arg )
      return arg
    self.size = build(size)
    self.pos = build(pos)
  
# TODO: compare using this list
languages = ('de','en','es','fr')

def buildsvg(filepath, langid, gname):
  
  print 'Opening', filename + '/' + filepath, '...'
  
  with krafile.open(filepath, "r") as r:
    xml = etree.parse(r).getroot()
    r.close()
  xml = noprefix(xml)
  root = copy.copy(rootsvg)
  
  title = etree.SubElement(root, 'title', id="SVGtitle")
  title.text = (img.title.split(langid))[0]
  
  SVGdesc      = etree.SubElement(root, "desc", id="SVGdesc")
  SVGdesc.text = u"\n{}\n  ".format(abstract)
  
  image = etree.SubElement(root, 'image', href = args.imageoutput)
  rect  = etree.SubElement(root, 'rect', opacity = '0')
  for i in [image,rect]:
    edic(i, { 'width' :'100%', 'height':'100%'})
  
  # TODO: use subgroup parent to style all dialog text elements.
  # TODO: if group has only one child, merge them.
  
  # class is reserved word, so must exist as string
  group = etree.SubElement(root, "g", {'class': gname} )
  
  if gname == 'dialog':
    group.set('font-family','kazual')
    group.set('text-anchor','middle')
  
  for frame in xml.findall('.//frame'):
    frame = noprefix(frame)
    textobj = textobject(frame)
    
    # For better centering. Alter pos based on width:
    if group.get('text-anchor') == 'middle':
      textobj.pos[0] += textobj.size[0]/2 # half of width
    
    text = etree.SubElement(group, "text")
    translate = "translate( {}, {} )".format(*textobj.pos)
    text.set('transform',translate)
    
    for span in frame.findall('.//*/span'):
      span = span.text if span.text != None else '!!NO TEXT!!'
      line = etree.SubElement(text, "tspan", dy="1em", x="0")
      line.text = unicode(span, "utf-8")
  return etree.tostring(root, pretty_print=True)
  
# <? nodetype="grouplayer" name="lang"> <?> <THIS/> </?> </?>
langpath = './/*[@name="lang"][@nodetype="grouplayer"]/*/*'

for group in maindoc.findall(langpath):
  id = group.attrib.get('name')
  for layer in group.findall('.//*[@nodetype="shapelayer"]'):
    gname = layer.attrib.get('name')
    folder = layer.attrib.get("filename") + ".shapelayer"
    path = img.title + "/layers/" + folder + "/content.xml"
    
    outfile = "lang-"+id+".svg"
    outpath = os.path.join(mainpath, outfile)
    
    svg = buildsvg(path, id, gname)
    
    print "Writing output to",outpath
    with open(outpath, "w") as o:
      o.write( svg )
      o.close()
    print "Done!"
  # TODO: restructure for loop and svg build function
  content = {}
  for layer in group.findall('.//*[@nodetype="shapelayer"]'):
    name = layer.attrib.get("name")
    folder = layer.attrib.get("filename") + ".shapelayer"
    path = img.title + "/layers/" + folder + "/content.xml"
    content.update({name:path})
  # def buildsvg(content,):
  # for name,xmlfile in content.iteritems():
  #   group = etree.SubElement(root, 'g')
  #   group.set('class'=name)
  #   text = etree.SubElement(group, 'text')